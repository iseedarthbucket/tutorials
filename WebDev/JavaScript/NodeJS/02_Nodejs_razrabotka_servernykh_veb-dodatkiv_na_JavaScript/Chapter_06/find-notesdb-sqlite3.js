exports.findNoteById = function(ts, callback) {
    var didOne = false;
    db.each("SELECT * FROM notes WHERE ts = ?",
        [ ts ],
        function(err, row) {
            if (err) {
                util.log('FAIL to retrieve now ' + err);
                callback(err, null);
            } else {
                if (!didOne) {
                    callback(null, row);
                    didOne =true;
                }
            }
        });
}