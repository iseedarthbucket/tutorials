exports.emptyNote = { "ts": "", author: "", note: ""};
exports.add = function(author, note, callback) {
    db.run("INSERT INFO notes ( ts, author, note) "+
        "VALUES ( ?, ? , ? );",
        [ new Date(), author, note ],
        function(error) {
            if (error) {
                util.log('FAIL to add ' + error);
                callback(error);
            } else
            callback(null);
        });
}