var util = require('util');
var sqlite3 = require('sqlite3');

sqlite3.verbose();

var db = undefined;

exports.connect = function(callback) {
    db = new sqlite3.Database("chap06.sqlite3",
        sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE,
        function(err) {
            if (err) {
                utils.log('FAIL on creating database ' + err);
                callback(err);
            } else
            callback(null)
        });
}
exports.disconnect = function(callback) {
    db.run("CREATE TABLE IF NOT EXISTS notes "+
        "(ts DATETIME, author VARCHAR(255), note TEXT)",
        function(err) {
            if (err) {
                util.log('FAIL on creating table ' + err);
                callback(error);
            } else
            callback(null);
        });
}