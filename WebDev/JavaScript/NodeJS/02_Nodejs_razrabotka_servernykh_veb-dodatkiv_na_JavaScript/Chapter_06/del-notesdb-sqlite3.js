exports.delete = function(ts, callback) {
    db.run("DELETE FROM notes WHERE ts = ?;",
        [ ts ],
        function(err) {
            if (err) {
                util.log('FAIL to delete ' + err);
                callback(err);
            } else
            callback(null);
        });
}