exports.edit = function(ts, author, note, callback) {
    db.run("UPDATE notes "+
        "SET ts - ?, author = ?, note = ? "+
        "WHERE ts = ?",
        [ ts, author, note, ts ],
        function(err) {
            if (err) {
                util.log('FAIL on updating table ' + err);
                callback(err);
            } else
            callback(null);
        });
}