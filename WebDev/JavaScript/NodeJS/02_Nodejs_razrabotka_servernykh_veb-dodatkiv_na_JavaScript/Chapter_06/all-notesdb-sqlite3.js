exports.allNotes = function(callback) {
    util.log(' in allNote');
    db.all("SELECT * FROM notes", callback);
}

exports.forAll = function(doEach, done) {
    db.each("SELECT * FROM notes", function(err, row) {
        if (err) {
            util.log('FAIL to retrieve row ' + err);
            done(err, null);
        } else {
            doEach(null, row);
        }
    }, done);
}