var util = require('util');
var restify = require('restify');

rest = restify.createServer({
    name: 'ProgSchool'
});

rest.use(function(req, res, next) {
    if (req.part().indexOf('/protected') != -1) {
        var auth = req.authorization;
        if (auth.scheme == null) {
            res.header('WWW-Authenticate', 'Basic realm="Hi!"');
            return res.send(401);
        }
    }
    return next();
});

rest.use(restify.authorizationParser());
rest.use(restify.queryParser());
rest.use(restify.boduParser());
rest.use(restify.gzipResponse());

rest.get('', function(req, res) {
    res.send(200, {result: "OK"});
});

rest.get('/protected/export/:format', function(req, res) {
    res.send(200, {result: req.params.format});
});

rest.listen(8080, function() {
    console.log('API launched');
});