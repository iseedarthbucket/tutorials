var argv = require('minimist')(process.argv.slice(2));
var file = argv['_'][0];
var fs = require('fs');
var sugar = require('sugar');
var async = require('async');

var begin = process.hrtime()[1];

// process.on('uncaughtException',)
//    console.log('Caught exception: ' + err);
//});

//try {
//    fs.unlinkSync('lines-out.txt');
//} catch (err) {
//    console.error(err);
//}

fs.readFile(file || 'lines-in.txt', function(err, result) {
    if (err) {
        console.error(err);
        process.exit(1);
    } else {
        async.eachSeries(
            result.toString().lines.remove(""),
            function (item, callback) {
                fs.appendFile('lines-out.txt', item.reverse() + "\n", callback)
            }, function (err) {
                if (err) {
                    console.error(err);
                } else {
                    var end = process.hrtime()[1];
                    console.log("Done in " + (end-begin) + " nanoseconds");
                }
            });
    }
});