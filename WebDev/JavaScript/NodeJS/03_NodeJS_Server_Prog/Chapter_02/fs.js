// Zavantazhennia modulia fs (failova systema)
var fs = require('fs');

// Chytannia vmistu faila v pamiati
fs.readFile('example_log.txt', function(err, logData) {
    // Yakshcho stalasia pomylka, my heneruiemo vyiniatok i prohrama zajinchuietsia
    if (err) throw err;

    // logData mai typ Buffer, perekladaiemo v string
    var text = logData.toString();
});