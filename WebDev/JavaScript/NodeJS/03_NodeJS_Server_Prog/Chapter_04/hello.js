// pidkliuchennia express
var express = require('express');
// stvorennia dodatku
var app = express();

// pidkliuchennia pidtrymky shablonizatorov
var templating = require('consolidate');
// vybyraiemo funktsiiu shablonizatsii dlia hbs
app.engine('hbs', templating.handlebars);
// za zamovchenniam vykorystovuiemo .hbs shablon
app.set('view engine', 'hbs');
// vkazuiemo dyrektoriiu dlia zavantazhennia shablona
app.set('views', __dirname + '/views');

// opratsovuiemo zapyty do holovnoi storinky
app.get('', function(req, res) {
    // vidobrazhennia dannykh na osnovi shablona
    res.render('hello', {
        title: 'Pryvit, handlebars!'
    });
});