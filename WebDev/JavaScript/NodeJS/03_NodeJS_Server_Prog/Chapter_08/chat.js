$(document).ready(function () {
    var socket = io.connect('http://localhost:8080');
    var messages = $("#messages");
    var message_txt = $("#message_text")
    var name = 'Bot_' + (Math.round(Math.random() * 10000));
    $('.chat .nick').text(name);

    function msg(nick, message) {
        var m = '<div class="msg">' +
                '<span class="user">' + safe(nick) + ':</span> '
                + safe(message) +
                '</div>';
        messages.append(m);
    }

    function msg_system(message) {
        messages.append($('<div>').addClass('msg system').text(message));
    }

    socket.on('connecting', function () {
        msg_system('Соединение...');
    });

    socket.on('connect', function () {
        msg_system('Соединение установлено!');
    });

    socket.on('message', function (data) {
        msg(data.name, data.message);
        message_txt.focus();
    });

    $("#message_btn").click(function () {
        var text = $("#message_text").val();
        if (text.length <= 0)
            return;
        message_txt.val("");
        socket.emit("message", {message: text, name: name});
    });

    function safe(str) {
        return str.replace(/&/g, '&amp;')
           .replace(/</g, '&lt;')
           .replace(/>/g, '&gt;');
    }
});