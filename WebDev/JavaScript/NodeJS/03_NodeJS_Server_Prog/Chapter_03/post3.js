function onRequest(request, response) {
    var postData = "";
    var pathname = url.parse(request.url).pathname;
    console.log("Request for " + pathname + " recived.");

    request.setEncoding("utf8");

    request.addListener("data", function(chunk) {
        postData += chunk;
        console.log("POST data chunk '" + chunk + "'.");
    });

    request.addListener("end", function() {
        route(handle, pathname, response, postData);
    });
}