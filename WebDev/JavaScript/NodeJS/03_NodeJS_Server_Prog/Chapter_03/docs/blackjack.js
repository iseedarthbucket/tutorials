var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin, // vvid z standartnoho potoka
    output: process.stdout // vyvid z standartnoho potoku
});
var fs = require('fs');

var handDealer = [];
var handPlayer = [];
var cash = 100;

// lohuiemo rezultaty
var logfile = process.argv[2];
function log(data) {
    if (logfile != undefined)
        fs.appendFileSync(logfile, data + '\n');
}

// dodaite vypadkovu kartu u ruku
function dealCard(hand) {
    var cardValues = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    hand.push(cardValues[Math.floor(Math.random() * 11)]);
}

// pidrakhovuie sumu baliv u rutsi
function calcSum(hand) {
    var sum = 0;

    // rakhuiemo vse, okrim tuziv
    for (var i=0; i<hand.length; i++) {
        var card = hand[i];
        if (card != "A") {
            if (card == "J" || card == "Q" || card == "K")
                sum = sum + 10;
            else
                sum = sum + parseInt(card);
        }
    }

    // tuz rakhuoetsia yak 1, yakshcho potochna suma bilshe 10, inakshe - yak 11
    for (var i=0; i<hand.length; i++) {
        var card = hand[i];
        if (card == "A") {
            if (sum > 10)
                sum = sum + 1;
            else
                sum = sum + 11;
        }
    }
    return sum;
}

console.log("Laskavo prosymo v kazino Tytanik!");
console.log("Vash balans " + cash + "$");
console.log("Natysnit klavishu Enter, shchob vidtvoryty, abo bud-yakyi symvol, yakyi potribno vyity")

function showHands() {
    // pokazaty stan hry
    console.log(
        handPlayer.join(" ") + " (" + calcSum(handPlayer) + ") <-> "
        + handDealer.join(" ") + " (" + calcSum(handDealer) + ")");
}

function checkWinner() {
    // pokazuiemo karty
    showHands();

    // povtoryty rezultat
    var player = calcSum(handPlayer);
    var dealer = calcSum(handDealer);
    if ((player <= 21) && ((player > dealer) || (dealer > 21))) {
        cash++;
        console.log("Vy vyhraly :) Teper u vas " + cash + "$");
        log("+");
    } else if (player == dealer) {
        console.log("Nichyia! U vas dosi " + cash + "$");
        log("=");
    } else {
        cash--;
        console.log("Vy prohraly :( Teper u vas " + cash + "$");
        log("-");
    }

    // hraiem shchie!
    game();
}

function choice() {
    showHands();

    rl.question('Dodaty kartu? Enter - tak, bud-yakyi symvol - nit.',
        function(answer) {
            if (answer == "") {
                // sdaiemo kartu hravtsiu
                dealCard(handPlayer, true);

                if (calcSum(handPlayer) < 21) {
                    // proponuiemo vziaty shchie odnu karu
                    choice();
                } else {
                    // shukaiemo peremozhtsia
                    checkWinner();
                }
            } else {
                // sdaiemo karty dileru
                while (calcSum(handDealer) < 18) {
                    dealerCard(handDealer);
                }

                // shukaiemo peremozhtsia
                checkWinner();
            }
        });
}

function game() {
    rl.question('Hraiemo? Enter - tak, bud-yakyi symvol - nit.',
        function(cmd) {
            if (cmd != "")
                process.exit();

            // na pochatku partii sdaiemo odnu kartu dileru, dvi hravtsiu
            handDealer = [];
            dealCard(handDealer);

            handPlayer = [];
            deakerCard(handPlayer);
            deakerCard(handPlayer);

            choice();
        });
};

game();