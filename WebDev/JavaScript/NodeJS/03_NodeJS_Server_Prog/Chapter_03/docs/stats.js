var fs = require('fs');
var logfile = process.argv[2];

fs.readFile(logfile, function(err, data) {
    if (err) {
        console.error('Pomylka pid chas chytannia failu: ' + err);
        console.dir(err);
        process.exit(1);
    }

    data = data.toString().split("\n");
    if (data.length == 0) {
        console.log("Nemaie shcho analizuvaty!");
        process.exit(1);
    }

    // vydaliaiemo ostannii porozhnii riadok
    data.pop()

    var wins = 0;
    var loses = 0;
    var draws = 0;

    var game = data;

    for (var i=0; i<data.length; i++) {
        if (data[i] == "+") {
            wins++;
        } else if (data[i] == "-") {
            loses++;
        } else {
            draws++;
        }
    }

    console.log("Usoho zihrano ihor: " + games.length);
    console.log("Peremoh: " + wins + ", nixhyikh: " + draws + ", porazok:" + loses);
    console.log("Vidsotok peremoh (bez urakhuvannia nizgyikh): " + (wins*100/loses).toFixed(2) + "%");
});