var urlutils = require('url');

// Rozbyvaiemo url na chastyny
var params = urlutils.parse(
    'http://user:pass@hosr.com:8080/p/a/t/h?query=string#hash', true);
console.log(params);

// zbyraiemo URL zi zminoiu parametriv
delete params.search;
params.query = {key1: 'value1', key2: 'value2'};

console.dir(urlutils.format(params));