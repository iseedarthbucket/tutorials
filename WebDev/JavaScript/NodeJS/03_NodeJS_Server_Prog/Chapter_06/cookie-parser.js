var express = require('express');
var cookieParser = require('cookie-parser');

express().use(cookieParser('optional secret string')).use(function(req, res, next) {
    res.end(JSON.stringify(req.cookies));
})